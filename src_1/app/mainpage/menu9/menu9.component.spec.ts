import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu9Page } from './menu9.page';

describe('Menu9Page', () => {
  let component: Menu9Page;
  let fixture: ComponentFixture<Menu9Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu9Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu9Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
