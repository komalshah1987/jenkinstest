import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu11Page } from './menu11.page';

describe('Menu11Page', () => {
  let component: Menu11Page;
  let fixture: ComponentFixture<Menu11Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu11Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu11Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
