import { Component, OnInit, Type, NgModuleFactory, Compiler } from '@angular/core';
import { ActivatedRoute, Routes, Route } from '@angular/router';
import { MenuService } from '../Services/menu.service';
import { Router } from '@angular/router';
//import { Page1Component } from './page1/page1.component';
import { TabMenuModule } from 'primeng/tabmenu';
import { MenuItem } from 'primeng/api';
import { Events } from '@ionic/angular';
import { Observable } from 'rxjs';
import { MenubarModule } from 'primeng/menubar';
import { MainPageService } from '../Services/MainPage.service';
@Component({
    selector: 'app-mainpage',
    templateUrl: './mainpage.page.html',
    styleUrls: ['./mainpage.page.scss'],
})
export class MainpagePage implements OnInit {
    UserName: string;
    ID: number;
    SubMenu: any;
    items: MenuItem[];
    MenuData: any;
    ParentID: any;
    //MenuId: number=1;
    constructor(private activatedroute: ActivatedRoute, private menuservice: MenuService,
        private router: Router, public events: Events, private _compiler: Compiler) {
        console.log('beforactive', activatedroute.routeConfig);
        this.events.publish('PageName', '');
    }

    ngOnInit() {
        debugger;
        var encrypted = localStorage.getItem('username');
        this.UserName = localStorage.getItem('username');
        
        this.UserName = encrypted;
        console.log(this.UserName);
        
        this.menuservice.getSubGroup(this.UserName).subscribe(data => {
            console.log("api", data);
            this.runRecursive(data);
            console.log(data);
            this.MenuData = data;
            console.log("Menudata1", this.MenuData);
        });
        
    }

    runRecursive(input) {
        if (input != null) {

            if (input.length > 0) {
                for (var i = 0, l = input.length; i < l; i++) {
                    var current = input[i];


                    if (current.command != null) {
                        var Id = current.ID
                        alert('this.addNewUser(' + Id + ')')

                        current.command = (onclick) => { this.addNewUser(`${Id}`) };

                    }
                    else {
                        current.command = (onclick) => { this.addNewUser('') };
                    }
                    this.runRecursive(current.items);
                };
            }
        }

    };


    //Add new user
    addNewUser(current) {
        debugger;
        if (current != "") {
            this.menuservice.getmenus(current, this.UserName)
                .subscribe(data => {
                    this.SubMenu = data;
                    console.log('PageName', this.SubMenu[0].MenuName);
                    this.events.publish('PageName', this.SubMenu[0].MenuName);
                    //this.MenuID = this.SubMenu[0].ID;
                });
        }
        else {
            this.SubMenu = null;
        }
    }
}

