import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu12Page } from './menu12.page';

describe('Menu12Page', () => {
  let component: Menu12Page;
  let fixture: ComponentFixture<Menu12Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu12Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu12Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
