import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu6Page } from './menu6.page';

describe('Menu6Page', () => {
  let component: Menu6Page;
  let fixture: ComponentFixture<Menu6Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu6Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu6Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
