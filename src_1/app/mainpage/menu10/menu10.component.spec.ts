import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu10Page } from './menu10.page';

describe('Menu10Page', () => {
  let component: Menu10Page;
  let fixture: ComponentFixture<Menu10Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu10Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu10Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
