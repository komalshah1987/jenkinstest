import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu5Page } from './menu5.page';

describe('Menu5Page', () => {
  let component: Menu5Page;
  let fixture: ComponentFixture<Menu5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu5Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
