import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { IonicModule } from '@ionic/angular';
import { routes } from './mainpage.routes';
import { IonicSelectableModule } from 'ionic-selectable';
// import { MatTableModule, MatSortModule } from '@angular/material';
// import { CdkTableModule } from '@angular/cdk/table';
//import { AgGridModule } from 'ag-grid-angular';
import { MainpagePage } from './mainpage.page';
import {CustomService} from '../Services/custom-service.service'
import { Data } from '../Services/data.service';
// import { MatPaginatorModule } from '@angular/material';
import {IonicStorageModule} from '@ionic/storage';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {TabMenuModule} from 'primeng/tabmenu';
import {
  MatTableModule, MatSortModule, MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatExpansionModule,
  

  MatButtonToggleModule,MatNativeDateModule,
} from '@angular/material';
//import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatRadioModule} from '@angular/material/radio';
import {MatGridListModule} from '@angular/material/grid-list';
import { CdkTableModule } from '@angular/cdk/table';
import{TreeTableModule} from 'primeng/treetable';
import {Menu4Page} from './menu4/menu4.component';
import {Menu3Page} from './menu3/menu3.component';

import {Menu9Page} from './menu9/menu9.component';
import {Menu10Page} from './menu10/menu10.component';
import {Menu11Page} from './menu11/menu11.component';
import {Menu12Page} from './menu12/menu12.component';
import { TableModule } from 'primeng/table';
import { Menu5Page } from './menu5/menu5.component';
import {MenubarModule} from 'primeng/menubar';
import {PanelMenuModule} from 'primeng/panelmenu';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      IonicModule,
      RouterModule.forChild(routes),
      DataTablesModule,
      IonicSelectableModule,      
      MatTableModule, MatSortModule, MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
      MatPaginatorModule,
      MatRippleModule,
      MatCardModule,
      MatProgressSpinnerModule,
      MatButtonToggleModule,
      MatIconModule,
      CdkTableModule,
      MatGridListModule,
      MatRadioModule,
      ReactiveFormsModule,MatExpansionModule,
      MatStepperModule,MatDatepickerModule,MatNativeDateModule,
      TreeTableModule,
      TableModule,
      PanelMenuModule,
      TabMenuModule,
      MenubarModule,
     //MatMomentDateModule,
      IonicStorageModule.forRoot()
//,
     //AgmCoreModule.forRoot({
     //   apiKey: 'XYZ',
      //  libraries: ['places']
      //}) 
    ],
    providers: [Data,CustomService
//,
      //{ provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } } 
],
  
    declarations: [MainpagePage,Menu4Page,Menu3Page,Menu5Page,Menu9Page,Menu10Page,Menu11Page,Menu12Page],
    entryComponents: []

})
export class MainpagePageModule {}
