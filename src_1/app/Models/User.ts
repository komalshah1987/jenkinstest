export interface IUser {
    Email_id: string;
    Error: string;
    FirstName: string;
    ID: number;
    LastName: string;
    Profile_Type: string;
    UserName: string;
}