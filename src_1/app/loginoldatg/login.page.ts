import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../Services/authentication.service';
import { LoginService } from '../Services/Login.service';
import { IUser } from '../Models/User';
import { Events } from '@ionic/angular';
import { Data } from "../Services/data.service";
import { error } from 'protractor';
import { EncrDecrServiceService } from '../Services/encr-decr-service.service';
import { debug } from 'util';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    public User: IUser;
    public error: string;
    constructor(private authentication: AuthenticationService, private loginservice: LoginService,private EncrDecr: EncrDecrServiceService,
        public events: Events,
        public data: Data) {
        this.events.publish('PageName', 'login');}

  ngOnInit() {
  }
    loginUser(e) {
        debugger;
        e.preventDefault();
        console.log(e);
        let token : any;
        // var username = this.EncrDecr.set('123456$#@$^@1ERF',e.target.elements[0].value);
        // var password = this.EncrDecr.set('123456$#@$^@1ERF',e.target.elements[1].value);

        var username=e.target.elements[0].value;
        var password=e.target.elements[1].value;
       
        this.loginservice.getUserDetails(username, password)
            .subscribe(data => {
            this.User = data;
            console.log(this.User);
            if (this.User.Error != null) {
                this.error = this.User.Error;
            }
            else {
                this.data.storage = {
                    "firstname": "Nic",
                    "lastname": "Raboy",
                    "address": {
                        "city": "San Francisco",
                        "state": "California"
                    }
                }
                this.authentication.login(username, password);
            }
            },
            error => {
                this.error = error;
            })
        


    }
    //login(username, password) {
    //    this.authentication.login(username, password);
    //}
}
