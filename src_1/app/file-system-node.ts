import { TreeNode } from 'primeng/api/treenode';

export class FileSystemNode implements TreeNode {
     //_name: String;
    _Q1: number = 0;
    _Q2: number = 0;
    _Q3: number = 0;
    _Q4: number = 0;
    _ratio: number = 100;
    //_total: number = 0;
    /* _fileType: String;*/
    //_visited: boolean;  
    data?: FileSystemNode;
    //children?: TreeNode[];

    children?: FileSystemNode[];
    parent?: TreeNode;
    leaf: boolean;
    type: string;
    key: string;


    constructor(data?: any,parent?:TreeNode, columns?: any[]) {
        debugger;
        this.data = data;
       
        if (parent) this.parent = parent;
        if (!data) return;

        let queue = [data];
        let nodeQueue: Array<TreeNode> = [this];

        while (queue.length > 0) {
            let dataNode = queue.shift();
            dataNode.visited = true; // Marking node as visited

            let node = nodeQueue.shift();
            node.data = new FileSystemNode();
            console.log(dataNode);

           /*  for (let i = 0; i < columns.length; i++) {
                node.data[columns[i]] = dataNode.data[columns[i].ColumnName];
              } */
            //node.data["plantid"] = dataNode.data["plantid"];
            node.data["Productid"] = dataNode.data["Productid"];
            node.data["RegionId"] = dataNode.data["RegionId"];
            //node.data["Q1"] = dataNode.data["Q1"];
            // node.data["Q2"] = dataNode.data["Q2"];
            // node.data["Q3"] = dataNode.data["Q3"];
            // node.data["Q4"] = dataNode.data["Q4"];
           // node.data.ProductId=dataNode.data["ProductId"];
            node.data._Q1=dataNode.data["Q1"];
            node.data._Q2=dataNode.data["Q2"];
            node.data._Q3=dataNode.data["Q3"];
            node.data._Q4=dataNode.data["Q4"];

           
            //node.data.RegionId=dataNode.data["RegionId"];
            //node.data.AdjustedQuantity=dataNode.data["Adjusted Quantity"];
            //node.data.BelurB2R=dataNode.data["Belur B2R"];
            //node.data.Level = dataNode.data["Level"];
      
            if (node.parent) {
                node.data.parent = node.parent;
                node.parent = null;
                node.data._ratio = (dataNode.data.Q1 / node.data.parent.data.Q1) * 100;
            } else {
                node.data._ratio = 100;
            }

            if (!dataNode.children) continue;
            node.data.children = [];
            node.children = [];
            for (let i = 0; i < dataNode.children.length; i++) {
                //if (!dataNode.children[i].visited) {
                    queue.push(dataNode.children[i]);
                    let child = new FileSystemNode(null, node);
                    node.children.push(child);
                    node.data.children.push(child); // Added this for root node children reference.
                    nodeQueue.push(node.children[i]);
                //}
            }
        }
    }

   
    _factorizeChildrens(ColumnName?:any) {
        // Distributing the values according to the existing child proportion
        // Distributing the values to the child nodes recursively using BFS.
        for (let i = 0; i < this.children.length; i++) {
            let queue = [this.children[i]];
            while (queue.length > 0) {
                let node = <FileSystemNode>queue.shift();
                if (!node.data) continue;
                // Updating the child node value
                if (node.parent) {
                    node.data["_"+ColumnName] = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data)[ColumnName];
                   //node.data._Q1 = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data).Q1;
                } else if (node.data.parent) {
                    node.data["_"+ColumnName] = (node.data._ratio / 100) * (<FileSystemNode>node.data.parent.data)[ColumnName];
                }
                if (!node.data.children) continue;
                for (let j = 0; j < node.data.children.length; j++) {
                    //if (!(<FileSystemNode>node.data.children[j])._visited) {
                        queue.push(node.data.children[j]);
                    //}
                }
            }
        }
    }

    _factorizeParentAndChildrens(ColumnName?:any) {
        let parent = this.parent;
        let queue = [this.parent];
        while (queue.length > 0) {
            let node = <FileSystemNode>queue.shift();
            if (!node.children) continue;
            let sum: number = 0;
            let total: number = node.children.reduce((sum, n) => {
                sum += n.data[ColumnName];
                return sum;
            }, sum);
            node.data["_"+ColumnName] = total;
            //node.data._Q1 = total;
            if (node.data.parent) {
                parent = node.data.parent;
                queue.push(node.data.parent)
            }
        }

        // Update the ratios of the child nodes recursively 
        if (parent.children && parent.children.length > 0) {
            for (let i = 0; i < parent.children.length; i++) {
                queue = [parent.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    node.data._ratio = (node.data["_"+ColumnName]  / node.data[ColumnName] ) * 100;
                    if (!node.data.children) continue;
                    for (let i = 0; i < node.data.children.length; i++) {
                        queue.push(node.data.children[i]);
                    }
                }
            }
        }


    }

    
    _factorizeParentAndChildrensMiddle(ColumnName?:any) {
        let parent = this.parent;
        let queue = [this.parent];
        while (queue.length > 0) {
            let node = <FileSystemNode>queue.shift();
            if (!node.children) continue;
            let sum: number = 0;
            let total: number = node.children.reduce((sum, n) => {
                sum += n.data[ColumnName];
                return sum;
            }, sum);
            node.data["_"+ColumnName] = total;
            if (node.data.parent) {
                parent = node.data.parent;
                queue.push(node.data.parent)
            }
        }

        for (let i = 0; i < this.children.length; i++) {
            let queue = [this.children[i]];
            while (queue.length > 0) {
                let node = <FileSystemNode>queue.shift();
                if (!node.data) continue;
                // Updating the child node value
                if (node.parent) {
                    node.data["_"+ColumnName]  = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data)[ColumnName];
                } else if (node.data.parent) {
                    node.data["_"+ColumnName]  = (node.data._ratio / 100) * (<FileSystemNode>node.data.parent.data)[ColumnName];
                }

                if (!node.data.children) continue;
                for (let j = 0; j < node.data.children.length; j++) {
                    //if (!(<FileSystemNode>node.data.children[j])._visited) {
                        queue.push(node.data.children[j]);
                    //}
                }
            }
        }

        // Update the ratios of the child nodes recursively 
        if (parent.children && parent.children.length > 0) {
            for (let i = 0; i < parent.children.length; i++) {
                queue = [parent.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    if (!node.parent) continue;
                    node.data._ratio = ( node.data["_"+ColumnName]  / node.parent.data[ColumnName]) * 100;
                    if (!node.data.children) continue;
                    for (let i = 0; i < node.data.children.length; i++) {
                        queue.push(node.data.children[i]);
                    }
                }
            }
        }
    }

    // set Q1(val: number)
    //  {
    // debugger;
    //     //Check for empty value -TO.DO
    //     this._Q1 = Number(val);
    //     // If there is no parent then its a root
    //     if (!this.parent) {
    //         this._factorizeChildrens();
    //     } else {
    //         // If there is no children then it is a leaf node, then update its parent values recursively.
    //         if (!this.children) {
    //             this._factorizeParentAndChildrens();
    //         } else {
    //             this._factorizeParentAndChildrensMiddle();
    //         }
    //     }
    // }

    // get Q1(): number {
    //     return this._Q1;
    // }

    set Q1(val: number) {
        debugger;
            //Check for empty value -TO.DO
            this._Q1 = Number(val);
            // If there is no parent then its a root
            if (!this.parent) {
                this._factorizeChildrens("Q1");
            } else {
                // If there is no children then it is a leaf node, then update its parent values recursively.
                if (!this.children) {
                    this._factorizeParentAndChildrens("Q1");
                } else {
                    this._factorizeParentAndChildrensMiddle("Q1");
                }
            }
        }
    
        get Q1(): number {
            return this._Q1;     
        }

        set Q2(val: number) {
            debugger;
                //Check for empty value -TO.DO
                this._Q2 = Number(val);
                // If there is no parent then its a root
                if (!this.parent) {
                    this._factorizeChildrens("Q2");
                } else {
                    // If there is no children then it is a leaf node, then update its parent values recursively.
                    if (!this.children) {
                        this._factorizeParentAndChildrens("Q2");
                    } else {
                        this._factorizeParentAndChildrensMiddle("Q2");
                    }
                }
            }
        
            get Q2(): number {
                return this._Q2;
            }

            set Q3(val: number) {
                debugger;
                    //Check for empty value -TO.DO
                    this._Q3 = Number(val);
                    // If there is no parent then its a root
                    if (!this.parent) {
                        this._factorizeChildrens("Q3");
                    } else {
                        // If there is no children then it is a leaf node, then update its parent values recursively.
                        if (!this.children) {
                            this._factorizeParentAndChildrens("Q3");
                        } else {
                            this._factorizeParentAndChildrensMiddle("Q3");
                        }
                    }
                }
            
                get Q3(): number {
                    return this._Q3;
                }

                set Q4(val: number) {
                    debugger;
                        //Check for empty value -TO.DO
                        this._Q4 = Number(val);
                        // If there is no parent then its a root
                        if (!this.parent) {
                            this._factorizeChildrens("Q4");
                        } else {
                            // If there is no children then it is a leaf node, then update its parent values recursively.
                            if (!this.children) {
                                this._factorizeParentAndChildrens("Q4");
                            } else {
                                this._factorizeParentAndChildrensMiddle("Q4");
                            }
                        }
                    }
                
                    get Q4(): number {
                        return this._Q4;
                    }

    get ratio(): number {
        return Number(this._ratio.toFixed(2));
    }

    set ratio(ratio: number) {
        this._ratio = ratio;
    }

    

}

      /*constructor(data?: any, children? :FileSystemNode[], parent?: TreeNode) {
        this.data = data;
        this.children = children;

       if (parent) this.parent = parent;
        if (!data) return;

        let queue = [data];
        let nodeQueue: Array<TreeNode> = [this];

        while (queue.length > 0) {
            let dataNode = queue.shift();
            dataNode.visited = true; // Marking node as visited

            let node = nodeQueue.shift();
            node.data = new FileSystemNode();
            node.data._name = dataNode.data.name;
            node.data._Q1 = dataNode.data.val;
            node.data._fileType = dataNode.data.fileType;
            if (node.parent) {
                node.data.parent = node.parent;
                node.parent = null;
                node.data._ratio = (dataNode.data.val / node.data.parent.data.val) * 100;
            } else {
                node.data._ratio = 100;
            }

            if (!dataNode.children) continue;
            node.data.children = [];
            node.children = [];
            for (let i = 0; i < dataNode.children.length; i++) {
                if (!dataNode.children[i].visited) {
                    queue.push(dataNode.children[i]);
                    let child = new FileSystemNode(null, node);
                    node.children.push(child);
                    node.data.children.push(child); // Added this for root node children reference.
                    nodeQueue.push(node.children[i]);
                }
            }
        } */
    

    /* get fileType(): String {
        return this._fileType;
    }

    set fileType(val: String) {
        this._fileType = val;
    }

    get total(): number {
        return this._total;
    }

    set total(total: number) {
        this._total = total;
    }

    set val(val: number) {
        this._Q1 = Number(val);
        // If there is no parent then its a root
        if (!this.parent) {
            // Distributing the values according to the existing child proportion
            // Distributing the values to the child nodes recursively using BFS.
            for (let i = 0; i < this.children.length; i++) {
                let queue = [this.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    if (!node.data) continue;
                    // Updating the child node value
                    if (node.parent) {
                        node.data._Q1 = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data).Q1;
                    } else if (node.data.parent) {
                        node.data._Q1 = (node.data._ratio / 100) * (<FileSystemNode>node.data.parent.data).Q1;
                    }
                    if (!node.data.children) continue;
                    for (let j = 0; j < node.data.children.length; j++) {
                        if (!(<FileSystemNode>node.data.children[j])._visited) {
                            queue.push(node.data.children[j]);
                        }
                    }
                }
            }
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                let parent = this.parent;
                let queue = [this.parent];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    if (!node.children) continue;
                    let sum: number = 0;
                    let total: number = node.children.reduce((sum, n) => {
                        sum += n.data.Q1;
                        return sum;
                    }, sum);
                    node.data._Q1 = total;
                    if (node.data.parent) {
                        parent = node.data.parent;
                        queue.push(node.data.parent)
                    }
                }

                // Update the ratios of the child nodes recursively 
                if (parent.children && parent.children.length > 0) {
                    for (let i = 0; i < parent.children.length; i++) {
                        queue = [parent.children[i]];
                        while (queue.length > 0) {
                            let node = <FileSystemNode>queue.shift();
                            node.data._ratio = (node.data._Q1 / node.parent.data.Q1) * 100;
                            if (!node.data.children) continue;
                            for (let i = 0; i < node.data.children.length; i++) {
                                queue.push(node.data.children[i]);
                            }
                        }
                    }
                }

            }
        }
    }

    get val(): number {
        return this._Q1;
    }

    get ratio(): number {
        return Number(this._ratio.toFixed(2));
    }

    set ratio(ratio: number) {
        this._ratio = ratio;
    }

    get name(): String {
        return this._name;
    }

    set name(val: String) {
        this._name = val;
    } */
