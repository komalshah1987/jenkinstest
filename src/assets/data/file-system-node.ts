
import { TreeNode } from 'primeng/api/treenode';


/*Changes for TreeTable */
/*
Change1:-Make a private property _Q1_2021_Qty
Change2:-Add property in if(columns[i].ColumnName != "Q1_2021"){
Change3:-Make setter property and pass property Q1_2021 in method 
  set Q1_2021(val: number) {
    //debugger;
        //Check for empty value -TO.DO
        this._Q1_2021_Qty = Number(val);
        // If there is no parent then its a root
        if (!this.parent) {
            this._factorizeChildrens("Q1_2021");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q1_2021");
            } else {
                this._factorizeParentAndChildrensMiddle("Q1_2021");
            }
        }
    }
Change4:-Make getter property 
    get Q1_2021(): number {
        return this._Q1_2021;
    }
*/  
//Q1_2021,Q1_2022,Q2_2020,Q2_2021,Q3_2020,Q3_2021,Q4_2020,Q4_2021
export class FileSystemNode implements TreeNode {
/*Change1*/ 
    _Q1_2021_Qty: number = 0;
    _Q1_2022_Qty: number = 0;
    _Q2_2020_Qty: number = 0;
    _Q2_2021_Qty: number = 0;
    _Q3_2020_Qty: number = 0;
    _Q3_2021_Qty: number = 0;
    _Q4_2020_Qty: number = 0;
    _Q4_2021_Qty: number = 0;

    _Q1_2021_Wgt: number = 0;
    _Q1_2022_Wgt: number = 0;
    _Q2_2020_Wgt: number = 0;
    _Q2_2021_Wgt: number = 0;
    _Q3_2020_Wgt: number = 0;
    _Q3_2021_Wgt: number = 0;
    _Q4_2020_Wgt: number = 0;
    _Q4_2021_Wgt: number = 0;


    _InitalValueQ1_2021: number =0;
    _ratio: number = 100;
    data?: FileSystemNode;
    children?: FileSystemNode[];
    parent?: TreeNode;
    leaf: boolean;
    type: string;
    key: string;
    dirtyCheck:boolean;
    columnList:string[];
    

    constructor(data?: any,parent?:TreeNode, columns?: any[]) {
        this.data = data;
        this.columnList=[];//List of editable columns
        if (parent) this.parent = parent;
        if (!data) return;

        let queue = [data];
        let nodeQueue: Array<TreeNode> = [this];

        while (queue.length > 0) {
            this.columnList=[];//List of editable columns
            let dataNode = queue.shift();
            dataNode.visited = true; // Marking node as visited

            let node = nodeQueue.shift();
            node.data = new FileSystemNode();
            columns.forEach(column=>{
                if(column.ColumnName!="Q1_2021_Qty" || column.ColumnName!="Q1_2022_Qty" ||
                column.ColumnName!="Q2_2020_Qty" || column.ColumnName!="Q2_2021_Qty" ||
                column.ColumnName!="Q3_2020_Qty" || column.ColumnName!="Q3_2021_Qty" ||
                column.ColumnName!="Q4_2020_Qty" || column.ColumnName!="Q4_2021_Qty"
                )// if(column.InputControls!="TextBox")
                 {
                   node.data[column.ColumnName] = dataNode.data[column.ColumnName];
                   var _columnName="_"+column.ColumnName;
                   //node.data[_columnName]=dataNode.data[column.ColumnName];
                   this.columnList.push(column.ColumnName);
                 }
             })
             node.data.dirtyCheck = false;
           
            node.data.ratio1={};
            node.data.style={};
            if (node.parent) {
                node.data.parent = node.parent;
                
                
                
                node.parent = null;
                //node.data._ratio = (dataNode.data.Q1_2021 / node.data.parent.data.Q1_2021) * 100;
            } else {
                columns.forEach(column=>{
                   node.data.ratio1[column.ColumnName]=100;

                });
                //node.data._ratio = 100;
            }
           // console.log("NODE",node.data);
            if (!dataNode.children) continue;
            node.data.children = [];
            node.children = [];
            for (let i = 0; i < dataNode.children.length; i++) {
                //if (!dataNode.children[i].visited) {
                    queue.push(dataNode.children[i]);
                    let child = new FileSystemNode(null, node);
                   
                    node.children.push(child);
                    node.data.children.push(child); // Added this for root node children reference.
                    nodeQueue.push(node.children[i]);
                //}
            }
        }
    }

   

    _factorizeChildrens(ColumnName?:any) {
        // Distributing the values according to the existing child proportion
        // Distributing the values to the child nodes recursively using BFS.
        if(typeof this.children!= undefined || this.children!=undefined)
        {
            for (let i = 0; i < this.children.length; i++) {
                let queue = [this.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    if (!node.data) continue;
                    // Updating the child node value
                    if (node.parent) {
                        node.data[ColumnName] = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data)[ColumnName];
                       //node.data._Q1_2021_Qty = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data).Q1_2021;
                    } else if (node.data.parent) {
                        node.data[ColumnName] = (node.data._ratio / 100) * (<FileSystemNode>node.data.parent.data)[ColumnName];
                    }
                    if (!node.data.children) continue;
                    for (let j = 0; j < node.data.children.length; j++) {
                        //if (!(<FileSystemNode>node.data.children[j])._visited) {
                            queue.push(node.data.children[j]);
                        //}
                    }
                }
            }
        }
        
    }

    _factorizeParentAndChildrens(ColumnName?:any) {
        let parent = this.parent;
        let queue = [this.parent];
        while (queue.length > 0) {
            let node = <FileSystemNode>queue.shift();
            if (!node.children) continue;
            let sum: number = 0;
            let total: number = node.children.reduce((sum, n) => {
               // if(n.data.type=="Consensus")
               console.log(n.data.type);
                    sum += n.data._Q1_2021_Qty;
                return sum;
            }, sum);
            //
            node.data._Q1_2021_Qty = total;
            //node.subtotal._Q1_2021_Qty=total;
            //node.subtotal.Q1_2021=total;
            if (node.data.parent) {
                parent = node.data.parent;
                queue.push(node.data.parent)
            }
        }

        // Update the ratios of the child nodes recursively 
        if (parent.children && parent.children.length > 0) {
            for (let i = 0; i < parent.children.length; i++) {
                queue = [parent.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    node.data._ratio = (node.data._Q1_2021_Qty / node.parent.data.Q1_2021) * 100;
                    if (!node.data.children) continue;
                    for (let i = 0; i < node.data.children.length; i++) {
                        queue.push(node.data.children[i]);
                    }
                }
            } 
        }


    }

    _factorizeParentAndChildrensMiddle(ColumnName?:any) {
        let parent = this.parent;
        let queue = [this.parent];
        debugger;
        while (queue.length > 0) {
            let node = <FileSystemNode>queue.shift();
           
            if (!node.children) continue;
            let sum: number = 0;
            let total: number = node.children.reduce((sum, n) => {
                    sum =parseFloat(sum+"")+ parseFloat(n.data[ColumnName]+"");
                return sum;
            }, sum);
            node.data["_"+ColumnName]=total;
            
node.data.dirtyCheck = true;
            if (node.data.parent) {
                parent = node.data.parent;
                queue.push(node.data.parent)
            }

            
        }
        debugger;
        for (let i = 0; i < this.children.length; i++) {
            let queue = [this.children[i]];
            while (queue.length > 0) {
                let node = <FileSystemNode>queue.shift();
                if (!node.data) continue;
                // Updating the child node value
                if (node.parent) {
                   // node.data._Q1_2021_Qty = (node.data._ratio / 100) * (<FileSystemNode>node.parent.data)._Q1_2021_Qty;
                } else if (node.data.parent) {
                   // node.data._Q1_2021_Qty = (node.data._ratio / 100) * (<FileSystemNode>node.data.parent.data)._Q1_2021_Qty;
                }
                
                if (!node.data.children) continue;
                for (let j = 0; j < node.data.children.length; j++) {
                    //if (!(<FileSystemNode>node.data.children[j])._visited) {
                        queue.push(node.data.children[j]);
                    //}
                }
            }
        }

        // Update the ratios of the child nodes recursively 
        if (parent.children && parent.children.length > 0) {
            for (let i = 0; i < parent.children.length; i++) {
                queue = [parent.children[i]];
                while (queue.length > 0) {
                    let node = <FileSystemNode>queue.shift();
                    
                    if (!node.parent) continue;
                    //node.data._ratio = (node.data._Q1_2021_Qty / node.parent.data.Q1_2021) * 100;
                    if (!node.data.children) continue;
                    for (let i = 0; i < node.data.children.length; i++) {
                        queue.push(node.data.children[i]);
                    }
                }
            }
        }
        
    }

    /*Change3 */
    set Q1_2021_Qty(val: number) {
        //Check for empty value -TO.DO
        this._Q1_2021_Qty = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q1_2021_Qty");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q1_2021_Qty");
            } else {
                this._factorizeParentAndChildrensMiddle("Q1_2021_Qty");
            }
        }
    }
    }

    get Q1_2021_Qty(): number {
        return this._Q1_2021_Qty;
    }

    set Q1_2022_Qty(val: number) {
       //Check for empty value -TO.DO
       this._Q1_2022_Qty = Number(val);
       if(!isNaN(Number(val)) && Number(val)  != 0){
           this.dirtyCheck = true;
       // If there is no parent then its a root
       if (!this.parent) {
           if(this.children != undefined)
               this._factorizeChildrens("Q1_2022_Qty");
       } else {
           // If there is no children then it is a leaf node, then update its parent values recursively.
           if (!this.children) {
               this._factorizeParentAndChildrens("Q1_2022_Qty");
           } else {
               this._factorizeParentAndChildrensMiddle("Q1_2022_Qty");
           }
       }
   }
    }

    get Q1_2022_Qty(): number {
        return this._Q1_2022_Qty;
    }

    set Q2_2020_Qty(val: number) {
        //Check for empty value -TO.DO
        this._Q2_2020_Qty = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q2_2020_Qty");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q2_2020_Qty");
            } else {
                this._factorizeParentAndChildrensMiddle("Q2_2020_Qty");
            }
        }
    }
    }
 
    get Q2_2020_Qty(): number {
         return this._Q2_2020_Qty;
    }

    set Q2_2021_Qty(val: number) {
        //Check for empty value -TO.DO
        this._Q2_2021_Qty = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q2_2021_Qty");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q2_2021_Qty");
            } else {
                this._factorizeParentAndChildrensMiddle("Q2_2021_Qty");
            }
        }
    }
    }
 
    get Q2_2021_Qty(): number {
         return this._Q2_2021_Qty;
    }

    set Q3_2020_Qty(val: number) {
        //Check for empty value -TO.DO
        this._Q3_2020_Qty = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q3_2020_Qty");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q3_2020_Qty");
            } else {
                this._factorizeParentAndChildrensMiddle("Q3_2020_Qty");
            }
        }
    }
    }
 
    get Q3_2020_Qty(): number {
         return this._Q3_2020_Qty;
    }

    set Q3_2021_Qty(val: number) {
        //Check for empty value -TO.DO
        this._Q3_2021_Qty = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q3_2021_Qty");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q3_2021_Qty");
            } else {
                this._factorizeParentAndChildrensMiddle("Q3_2021_Qty");
            }
        }
    }
    }
 
    get Q3_2021_Qty(): number {
         return this._Q3_2021_Qty;
    }

    set Q4_2020_Qty(val: number) {
        //Check for empty value -TO.DO
        this._Q4_2020_Qty = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q4_2020_Qty");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q4_2020_Qty");
            } else {
                this._factorizeParentAndChildrensMiddle("Q4_2020_Qty");
            }
        }
    }
    }
 
    get Q4_2020_Qty(): number {
         return this._Q4_2020_Qty;
    }

    set Q4_2021_Qty(val: number) {
        //Check for empty value -TO.DO
        this._Q4_2021_Qty = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q4_2021_Qty");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q4_2021_Qty");
            } else {
                this._factorizeParentAndChildrensMiddle("Q4_2021_Qty");
            }
        }
    }
    }
 
    get Q4_2021_Qty(): number {
         return this._Q4_2021_Qty;
    }



    /********************Tons*********************** */
     /*Change3 */
     set Q1_2021_Wgt(val: number) {
        //Check for empty value -TO.DO
        this._Q1_2021_Wgt = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q1_2021_Wgt");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q1_2021_Wgt");
            } else {
                this._factorizeParentAndChildrensMiddle("Q1_2021_Wgt");
            }
        }
    }
    }

    get Q1_2021_Wgt(): number {
        return this._Q1_2021_Wgt;
    }

    set Q1_2022_Wgt(val: number) {
       //Check for empty value -TO.DO
       this._Q1_2022_Wgt = Number(val);
       if(!isNaN(Number(val)) && Number(val)  != 0){
           this.dirtyCheck = true;
       // If there is no parent then its a root
       if (!this.parent) {
           if(this.children != undefined)
               this._factorizeChildrens("Q1_2022_Wgt");
       } else {
           // If there is no children then it is a leaf node, then update its parent values recursively.
           if (!this.children) {
               this._factorizeParentAndChildrens("Q1_2022_Wgt");
           } else {
               this._factorizeParentAndChildrensMiddle("Q1_2022_Wgt");
           }
       }
   }
    }

    get Q1_2022_Wgt(): number {
        return this._Q1_2022_Wgt;
    }

    set Q2_2020_Wgt(val: number) {
        //Check for empty value -TO.DO
        this._Q2_2020_Wgt = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q2_2020_Wgt");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q2_2020_Wgt");
            } else {
                this._factorizeParentAndChildrensMiddle("Q2_2020_Wgt");
            }
        }
    }
    }
 
    get Q2_2020_Wgt(): number {
         return this._Q2_2020_Wgt;
    }

    set Q2_2021_Wgt(val: number) {
        //Check for empty value -TO.DO
        this._Q2_2021_Wgt = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q2_2021_Wgt");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q2_2021_Wgt");
            } else {
                this._factorizeParentAndChildrensMiddle("Q2_2021_Wgt");
            }
        }
    }
    }
 
    get Q2_2021_Wgt(): number {
         return this._Q2_2021_Wgt;
    }

    set Q3_2020_Wgt(val: number) {
        //Check for empty value -TO.DO
        this._Q3_2020_Wgt = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q3_2020_Wgt");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q3_2020_Wgt");
            } else {
                this._factorizeParentAndChildrensMiddle("Q3_2020_Wgt");
            }
        }
    }
    }
 
    get Q3_2020_Wgt(): number {
         return this._Q3_2020_Wgt;
    }

    set Q3_2021_Wgt(val: number) {
        //Check for empty value -TO.DO
        this._Q3_2021_Wgt = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q3_2021_Wgt");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q3_2021_Wgt");
            } else {
                this._factorizeParentAndChildrensMiddle("Q3_2021_Wgt");
            }
        }
    }
    }
 
    get Q3_2021_Wgt(): number {
         return this._Q3_2021_Wgt;
    }

    set Q4_2020_Wgt(val: number) {
        //Check for empty value -TO.DO
        this._Q4_2020_Wgt = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q4_2020_Wgt");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q4_2020_Wgt");
            } else {
                this._factorizeParentAndChildrensMiddle("Q4_2020_Wgt");
            }
        }
    }
    }
 
    get Q4_2020_Wgt(): number {
         return this._Q4_2020_Wgt;
    }

    set Q4_2021_Wgt(val: number) {
        //Check for empty value -TO.DO
        this._Q4_2021_Wgt = Number(val);
        if(!isNaN(Number(val)) && Number(val)  != 0){
            this.dirtyCheck = true;
        // If there is no parent then its a root
        if (!this.parent) {
            if(this.children != undefined)
                this._factorizeChildrens("Q4_2021_Wgt");
        } else {
            // If there is no children then it is a leaf node, then update its parent values recursively.
            if (!this.children) {
                this._factorizeParentAndChildrens("Q4_2021_Wgt");
            } else {
                this._factorizeParentAndChildrensMiddle("Q4_2021_Wgt");
            }
        }
    }
    }
 
    get Q4_2021_Wgt(): number {
         return this._Q4_2021_Wgt;
    }

    get ratio(): number {
        return this._ratio;
       // return Number(this._ratio.toFixed(2));
    }

    set ratio(ratio: number) {
        this._ratio = ratio;
    }

}

     
