import { Component, OnInit } from '@angular/core';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {


    constructor(public events: Events) {
        this.events.publish('PageName', 'Profile');}

  ngOnInit() {
  }

}
