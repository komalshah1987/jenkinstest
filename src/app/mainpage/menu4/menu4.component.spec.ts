import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu4Page } from './menu4.page';

describe('Menu4Page', () => {
  let component: Menu4Page;
  let fixture: ComponentFixture<Menu4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu4Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
