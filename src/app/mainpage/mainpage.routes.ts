import { Routes } from '@angular/router';
import { MainpagePage } from './mainpage.page';
import {Menu3Page} from './menu3/menu3.component';
import {Menu4Page} from './menu4/menu4.component';
import {Menu5Page} from './menu5/menu5.component';
import {Menu9Page} from './menu9/menu9.component';
import {Menu10Page} from './menu10/menu10.component';
import {Menu11Page} from './menu11/menu11.component';
import {Menu12Page} from './menu12/menu12.component';

export const routes: Routes = [
{
 path: 'tabs',
 component: MainpagePage,
children: [
    {
        path: '3',
        component: Menu3Page
        },
{
path: '4',
component: Menu4Page
},
{
    path: '5',
    component: Menu5Page
    },
{
path: '9',
component: Menu9Page
},
{
path: '10',
component: Menu10Page
},
{
path: '11',
component: Menu11Page
},
{
path: '12',
component: Menu12Page
},

 ]
}
];
