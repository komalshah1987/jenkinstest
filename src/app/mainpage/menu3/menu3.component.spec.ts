import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu3Page } from './menu3.page';

describe('Menu3Page', () => {
  let component: Menu3Page;
  let fixture: ComponentFixture<Menu3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
