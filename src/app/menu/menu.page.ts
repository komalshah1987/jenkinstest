import { Component, OnInit } from '@angular/core';
import { MenuService } from '../Services/menu.service';
import { Router, RouterEvent } from '@angular/router';
import { Data } from "../Services/data.service";
import { Events } from '@ionic/angular';
import {PanelMenuModule} from 'primeng/panelmenu';
import {MenuItem} from 'primeng/api';
//import { EncrDecrServiceService } from '../Services/encr-decr-service.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
    MenuData: any;
    UserName: string;
    items: MenuItem[];
    constructor(private menuservice: MenuService, private router: Router,
        private data: Data, public events: Events
    ) {
        this.events.publish('PageName', '');
       // alert(JSON.stringify(this.data.storage));
      // alert('Menu');
    }

    ngOnInit() {
        var encrypted =  localStorage.getItem('username');
       // var decrypted = this.EncrDecr.get('123456$#@$^@1ERF', encrypted);
         this.UserName = encrypted;
         
        //this.UserName = localStorage.getItem('username');
        console.log(this.UserName);
        //this.UserName = 'Admin';
        this.menuservice.getSubGroup(this.UserName).subscribe(data => {
            console.log("api", data);
            this.runRecursive(data);
            this.MenuData = data;
            console.log("Menudata1", this.MenuData);
        });
    }
   open(url:string,ID:number,MenuName:string)
   {
       localStorage.setItem("MenuName",MenuName);
       this.router.navigateByUrl(url+'?id='+ID);
   }


   addNewUser(current) {
    debugger;
   // alert(current);
   /*  if (current != "") {
        this.menuservice.getmenus(current, this.UserName)
            .subscribe(data => {
                this.SubMenu = data;
                console.log('PageName', this.SubMenu[0].MenuName);
                this.events.publish('PageName', this.SubMenu[0].MenuName);
                //this.MenuID = this.SubMenu[0].ID;
            });
    }
    else {
        this.SubMenu = null;
    } */
}

   runRecursive(input) {
    if (input != null) {

        if (input.length > 0) {
            for (var i = 0, l = input.length; i < l; i++) {
                var current = input[i];


                if (current.command != null) {
                    var Id = current.ID
                   

                    current.command = (onclick) => { this.addNewUser(`${Id}`) };

                }
                else {
                    current.command = (onclick) => { this.addNewUser('') };
                }
                this.runRecursive(current.items);
            };
        }
    }

};

}
