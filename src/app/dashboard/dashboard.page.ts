import { Component, OnInit } from '@angular/core';
import { MainPageService } from '../Services/MainPage.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
UserName:string;
  constructor(private mainpageservice: MainPageService,private router: Router) { }

  ngOnInit() {
    
    let dashboardMenu:any;
    this.UserName = localStorage.getItem('username');
    this.mainpageservice.GetDashboardMenu(this.UserName).subscribe(resp=>{
   
      dashboardMenu=resp;
      if(resp!="0")
      {
        this.router.navigate(['menu/dashboard/'+dashboardMenu]);
      }
     else
     {
       this.router.navigate(['menu/first']);
     }
     
    });
  }

}
