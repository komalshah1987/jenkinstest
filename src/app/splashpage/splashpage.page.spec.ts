import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplashpagePage } from './splashpage.page';

describe('SplashpagePage', () => {
  let component: SplashpagePage;
  let fixture: ComponentFixture<SplashpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplashpagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplashpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
