import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu10Component } from './menu10.component';

describe('Menu10Component', () => {
  let component: Menu10Component;
  let fixture: ComponentFixture<Menu10Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu10Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
