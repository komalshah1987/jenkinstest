import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu12Component } from './menu12.component';

describe('Menu12Component', () => {
  let component: Menu12Component;
  let fixture: ComponentFixture<Menu12Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu12Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
