import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu24Component } from './menu24.component';

describe('Menu24Component', () => {
  let component: Menu24Component;
  let fixture: ComponentFixture<Menu24Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu24Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu24Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
