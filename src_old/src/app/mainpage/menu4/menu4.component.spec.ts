import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu4Component } from './menu4.component';

describe('Menu4Component', () => {
  let component: Menu4Component;
  let fixture: ComponentFixture<Menu4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu4Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
