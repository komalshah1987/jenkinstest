import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu9Component } from './menu9.component';

describe('Menu9Component', () => {
  let component: Menu9Component;
  let fixture: ComponentFixture<Menu9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu9Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
