import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu15Component } from './menu15.component';

describe('Menu15Component', () => {
  let component: Menu15Component;
  let fixture: ComponentFixture<Menu15Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu15Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu15Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
