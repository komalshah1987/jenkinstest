import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu26Component } from './menu26.component';

describe('Menu26Component', () => {
  let component: Menu26Component;
  let fixture: ComponentFixture<Menu26Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu26Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu26Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
