import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu14Component } from './menu14.component';

describe('Menu14Component', () => {
  let component: Menu14Component;
  let fixture: ComponentFixture<Menu14Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu14Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu14Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
