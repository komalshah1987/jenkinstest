import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu37Component } from './menu37.component';

describe('Menu37Component', () => {
  let component: Menu37Component;
  let fixture: ComponentFixture<Menu37Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu37Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu37Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
