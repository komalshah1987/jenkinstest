import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu34Component } from './menu34.component';

describe('Menu34Component', () => {
  let component: Menu34Component;
  let fixture: ComponentFixture<Menu34Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu34Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu34Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
