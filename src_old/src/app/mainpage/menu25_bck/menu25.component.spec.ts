import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu25Component } from './menu25.component';

describe('Menu25Component', () => {
  let component: Menu25Component;
  let fixture: ComponentFixture<Menu25Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu25Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu25Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
