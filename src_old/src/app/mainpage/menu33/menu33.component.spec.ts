import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu33Component } from './menu33.component';

describe('Menu33Component', () => {
  let component: Menu33Component;
  let fixture: ComponentFixture<Menu33Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu33Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu33Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
