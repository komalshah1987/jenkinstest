import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu28Component } from './menu28.component';

describe('Menu28Component', () => {
  let component: Menu28Component;
  let fixture: ComponentFixture<Menu28Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu28Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu28Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
