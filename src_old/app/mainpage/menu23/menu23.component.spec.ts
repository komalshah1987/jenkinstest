import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu23Component } from './menu23.component';

describe('Menu23Component', () => {
  let component: Menu23Component;
  let fixture: ComponentFixture<Menu23Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu23Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu23Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
