import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu20Component } from './menu20.component';

describe('Menu20Component', () => {
  let component: Menu20Component;
  let fixture: ComponentFixture<Menu20Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu20Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu20Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
