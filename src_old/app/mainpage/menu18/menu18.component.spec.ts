import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu18Component } from './menu18.component';

describe('Menu18Component', () => {
  let component: Menu18Component;
  let fixture: ComponentFixture<Menu18Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu18Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu18Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
