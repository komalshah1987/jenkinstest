import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu36Component } from './menu36.component';

describe('Menu36Component', () => {
  let component: Menu36Component;
  let fixture: ComponentFixture<Menu36Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu36Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu36Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
