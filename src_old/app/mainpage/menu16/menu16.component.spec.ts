import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu16Component } from './menu16.component';

describe('Menu16Component', () => {
  let component: Menu16Component;
  let fixture: ComponentFixture<Menu16Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu16Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu16Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
