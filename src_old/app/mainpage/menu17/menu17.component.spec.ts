import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu17Component } from './menu17.component';

describe('Menu17Component', () => {
  let component: Menu17Component;
  let fixture: ComponentFixture<Menu17Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu17Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu17Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
