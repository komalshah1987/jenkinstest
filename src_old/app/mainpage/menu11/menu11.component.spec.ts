import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu11Component } from './menu11.component';

describe('Menu11Component', () => {
  let component: Menu11Component;
  let fixture: ComponentFixture<Menu11Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu11Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
