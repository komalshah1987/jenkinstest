import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu8Component } from './menu8.component';

describe('Menu8Component', () => {
  let component: Menu8Component;
  let fixture: ComponentFixture<Menu8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu8Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
