(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-with-otp-login-with-otp-module"],{

/***/ "./src/app/login-with-otp/login-with-otp.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/login-with-otp/login-with-otp.module.ts ***!
  \*********************************************************/
/*! exports provided: LoginWithOtpPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginWithOtpPageModule", function() { return LoginWithOtpPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _login_with_otp_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login-with-otp.page */ "./src/app/login-with-otp/login-with-otp.page.ts");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm5/snack-bar.es5.js");












var routes = [
    {
        path: '',
        component: _login_with_otp_page__WEBPACK_IMPORTED_MODULE_9__["LoginWithOtpPage"]
    }
];
var LoginWithOtpPageModule = /** @class */ (function () {
    function LoginWithOtpPageModule() {
    }
    LoginWithOtpPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _angular_material_stepper__WEBPACK_IMPORTED_MODULE_5__["MatStepperModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_7__["MatFormFieldModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"], _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_11__["MatSnackBarModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_login_with_otp_page__WEBPACK_IMPORTED_MODULE_9__["LoginWithOtpPage"]]
        })
    ], LoginWithOtpPageModule);
    return LoginWithOtpPageModule;
}());



/***/ }),

/***/ "./src/app/login-with-otp/login-with-otp.page.html":
/*!*********************************************************!*\
  !*** ./src/app/login-with-otp/login-with-otp.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Loader Starts-->\n<div *ngIf=\"show\" id=\"cover\">\n  <div ng-show=\"showLoader\" class=\"center\">\n      <div class=\"message\">\n          Please wait ... <br>\n          <svg width=\"55px\" height=\"67px\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\" class=\"lds-ellipsis\" style=\"background: none;\"><!--circle(cx=\"16\",cy=\"50\",r=\"10\")--><circle cx=\"84\" cy=\"50\" r=\"0\" fill=\"#ffcc33\"><animate attributeName=\"r\" values=\"10;0;0;0;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"84;84;84;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle><circle cx=\"42.6958\" cy=\"50\" r=\"10\" fill=\"#7de6ea\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.7s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.7s\"></animate></circle><circle cx=\"16\" cy=\"50\" r=\"7.85172\" fill=\"#ffa570\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.35s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.35s\"></animate></circle><circle cx=\"84\" cy=\"50\" r=\"2.14828\" fill=\"#d4e683\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle><circle cx=\"76.6958\" cy=\"50\" r=\"10\" fill=\"#ffcc33\"><animate attributeName=\"r\" values=\"0;0;10;10;10\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"16;16;16;50;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle></svg>\n\n      </div>\n  </div>\n</div>\n<!-- Loader Ends-->\n\n<ion-header>\n    <ion-toolbar class=\"new-background-color\" align-title=\"center\">\n        <ion-title >Login</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content  class=\"background\">\n    <!--<div id=\"webchat\" role=\"main\"></div>\n    <script src=\"https://cdn.botframework.com/botframework-webchat/latest/webchat.js\"></script>\n    <script>\n      window.WebChat.renderWebChat({\n        directLine: window.WebChat.createDirectLine({ token: 'qQDaXfBLHV0.cwA.U_w.CghIxhXTBl2cG343W0hris51Qq8RZPK4XrM_YjPwH0I' }),\n        userID: 'YOUR_USER_ID'\n      }, document.getElementById('webchat'));\n  </script>-->\n  <ion-grid>\n    <ion-row>\n        <ion-col class=\"showcase\">\n            <h1 text-center>Manage Your Business Account</h1>\n            <ion-img text-center src=\"../assets/images/TLogin.jpg\"></ion-img>\n        </ion-col>\n        <ion-col class=\"login-side\">\n            <h2 class=\"login-label\" text-center>ATG</h2>\n            <ion-card color=\"#d7eceb\" margin padding>\n                <ion-card-header>\n                    <ion-card-subtitle text-center>\n                        <!-- Enter details to Login -->\n                    </ion-card-subtitle>\n                </ion-card-header>\n              <ion-card-content>\n\n                    <mat-horizontal-stepper linear #stepper>\n                            <mat-step [stepControl]=\"firstFormGroup\">\n                              <form [formGroup]=\"firstFormGroup\" #emailForm>\n                                <div><h2>Login</h2></div>\n                                <mat-form-field  appearance=\"outline\">\n                                  <mat-label>Email</mat-label>\n                                  <input matInput placeholder=\"Email\" formControlName=\"firstCtrl\" #username required>\n                                 \n                                </mat-form-field>\n                                <div style=\"text-align:center;\">\n                                      \n                                  <button style=\"width:100%\" mat-flat-button matStepperNext (click)=\"SendOtp(username.value)\">Send OTP</button>\n                                </div>\n                              </form>\n                            </mat-step>\n                            <mat-step [stepControl]=\"secondFormGroup\" [optional]=\"isOptional\">\n                              <form [formGroup]=\"secondFormGroup\" #loginForm>\n                                    <div>Enter OTP you received on {{username.value}}</div>\n\n                                <mat-form-field>\n                                    <mat-label>OTP</mat-label>\n                                  <input matInput placeholder=\"OTP\"  #otp formControlName=\"secondCtrl\" required>\n                                </mat-form-field>\n                                <div>\n                                  <button mat-button matStepperPrevious>Back</button>\n                                  <button mat-button style=\"margin-left:10px;\" (click)=\"LoginWithOTP(otp.value)\">Login</button>\n                                </div>\n                              </form>\n                            </mat-step>\n                            \n                          </mat-horizontal-stepper>\n                          <div  style=\"text-align: center\">\n                              <a  [routerLink]=\"['/registration/tabs']\">New User? Register here!</a>\n                          </div>\n                       \n           \n            </ion-card-content>\n        </ion-card>\n       \n                <div  style=\"text-align: center;margin-top: 20px;\">\n                <a  [routerLink]=\"['/login']\">Login as Admin</a>\n                </div>\n           \n            \n               \n        \n    </ion-col>\n</ion-row>\n</ion-grid>\n<ion-grid class=\"blank\">\n    <ion-row>\n        <ion-col>\n        </ion-col>\n    </ion-row>\n</ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/login-with-otp/login-with-otp.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/login-with-otp/login-with-otp.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#cover {\n  position: fixed;\n  top: 0;\n  left: 0;\n  background: rgba(0, 0, 0, 0.35);\n  z-index: 5;\n  width: 100%;\n  height: 500%;\n  cursor: wait; }\n\n.message {\n  background: -webkit-linear-gradient(top, #ffffff, #fdf8ff);\n  position: absolute;\n  -webkit-transform: translate(89%, 49%);\n          transform: translate(89%, 49%);\n  width: 35%;\n  text-align: center;\n  padding: 3%;\n  box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); }\n\n.new-background-color {\n  --background: #ffb01a; }\n\nion-content {\n  font-family: Arial, Helvetica, sans-serif !important; }\n\nion-title {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  text-align: center; }\n\nion-text, ion-col {\n  margin: auto; }\n\n.showcase {\n  border-right: 1px solid gray;\n  width: 50% !important; }\n\n.login-side {\n  width: 50% !important; }\n\nion-card {\n  width: 70%;\n  margin: auto; }\n\nion-icon {\n  zoom: 1.3;\n  margin-right: 2px;\n  color: black; }\n\nion-button {\n  --color:#ffffff !important; }\n\nh1 {\n  font-size: 40px;\n  margin-bottom: 20px;\n  opacity: 0.5; }\n\nion-img {\n  height: 400px;\n  width: auto;\n  margin: auto; }\n\nion-button[type='submit'] {\n  -moz-appearance: none;\n  -webkit-appearance: none; }\n\n.submit-btn {\n  --background: #ffb01a;\n  --color: #161b12; }\n\nbutton {\n  background-color: #0cd1e8;\n  color: white; }\n\n::ng-deep .mat-horizontal-stepper-header-container {\n  display: none !important; }\n\n.mat-horizontal-content-container, .mat-stepper-horizontal {\n  background-color: var(--ion-color-base); }\n\n.mat-form-field {\n  width: 100%; }\n\n/*:host {\n    ion-content {\n        --background: linear-gradient(135deg, var(--ion-color-dark), var(--ion-color-primary));\n    }\n}\n\n.paz {\n    position: relative;\n    z-index: 10;\n}*/\n\n@media screen and (max-width: 760px) {\n  .login-side {\n    width: 100%; }\n  .showcase {\n    display: none;\n    width: 0; }\n  ion-card {\n    width: 95%; } }\n\n.blank {\n  height: 20vh; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4td2l0aC1vdHAvRTpcXEtvbWFsXFxEIERyaXZlXFxJb25pYyBQcm9qZWN0c1xcSW9uaWNQV0FBVEdcXGlvbmljNHB3YS9zcmNcXGFwcFxcbG9naW4td2l0aC1vdHBcXGxvZ2luLXdpdGgtb3RwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbG9naW4td2l0aC1vdHAvbG9naW4td2l0aC1vdHAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksZUFBZTtFQUNmLE1BQU07RUFDTixPQUFPO0VBQ1AsK0JBQTRCO0VBQzVCLFVBQVU7RUFDVixXQUFXO0VBQ1gsWUFBWTtFQUVaLFlBQVksRUFBQTs7QUFHaEI7RUFFSSwwREFBMEQ7RUFDMUQsa0JBQWtCO0VBQ2xCLHNDQUE4QjtVQUE5Qiw4QkFBOEI7RUFDOUIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsNkVBQXdFLEVBQUE7O0FBRTVFO0VBQ0kscUJBQWEsRUFBQTs7QUFFakI7RUFDSSxvREFBb0QsRUFBQTs7QUFFeEQ7RUFDSSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLE9BQU87RUFDUCxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUV0QjtFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDSSw0QkFBMkI7RUFDM0IscUJBQW1CLEVBQUE7O0FBRXZCO0VBQ0kscUJBQW1CLEVBQUE7O0FBTXZCO0VBQ0ksVUFBUztFQUNULFlBQVcsRUFBQTs7QUFFZjtFQUNJLFNBQVM7RUFDVCxpQkFBZ0I7RUFDaEIsWUFBVyxFQUFBOztBQUVmO0VBQ0ksMEJBQVEsRUFBQTs7QUFFWjtFQUNJLGVBQWM7RUFDZCxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBOztBQVFoQjtFQUNJLGFBQWE7RUFDYixXQUFXO0VBQ1gsWUFBVyxFQUFBOztBQUVmO0VBQ0kscUJBQXFCO0VBQ3JCLHdCQUF3QixFQUFBOztBQUU1QjtFQUNJLHFCQUFhO0VBQ2IsZ0JBQVEsRUFBQTs7QUFFWjtFQUVJLHlCQUF3QjtFQUN4QixZQUFXLEVBQUE7O0FBR2Y7RUFDSSx3QkFBd0IsRUFBQTs7QUFHMUI7RUFFRSx1Q0FBdUMsRUFBQTs7QUFHekM7RUFFRSxXQUFXLEVBQUE7O0FBRWY7Ozs7Ozs7OztFQ1pFOztBRHNCRjtFQUNJO0lBQ0ksV0FBVSxFQUFBO0VBRWQ7SUFDSSxhQUFhO0lBQ2IsUUFBTyxFQUFBO0VBRVg7SUFDSSxVQUFVLEVBQUEsRUFDYjs7QUFFTDtFQUNJLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xvZ2luLXdpdGgtb3RwL2xvZ2luLXdpdGgtb3RwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG4jY292ZXIge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDAuMzUpO1xuICAgIHotaW5kZXg6IDU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA1MDAlO1xuICAgIC8vZGlzcGxheTpub25lO1xuICAgIGN1cnNvcjogd2FpdDtcbn1cblxuLm1lc3NhZ2Uge1xuICAgIC8vIGJhY2tncm91bmQtaW1hZ2U6IHdoaXRlO1xuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDg5JSwgNDklKTtcbiAgICB3aWR0aDogMzUlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAzJTtcbiAgICBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwwLDAsMC4zMCksIDAgMTVweCAxMnB4IHJnYmEoMCwwLDAsMC4yMik7XG59XG4ubmV3LWJhY2tncm91bmQtY29sb3Ige1xuICAgIC0tYmFja2dyb3VuZDogI2ZmYjAxYTtcbn1cbmlvbi1jb250ZW50e1xuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XG59XG5pb24tdGl0bGUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLXRleHQsaW9uLWNvbHtcbiAgICBtYXJnaW46IGF1dG87XG59XG4uc2hvd2Nhc2V7XG4gICAgYm9yZGVyLXJpZ2h0OjFweCBzb2xpZCBncmF5O1xuICAgIHdpZHRoOjUwJSFpbXBvcnRhbnQ7XG59XG4ubG9naW4tc2lkZXtcbiAgICB3aWR0aDo1MCUhaW1wb3J0YW50O1xuICAgIFxufVxuaW9uLWdyaWR7XG4gICAgLy9iYWNrZ3JvdW5kOiB1cmwoLi5cXC4uXFxhc3NldHNcXGltYWdlc1xcTG9naW4tc2lkZS5qcGcpO1xufVxuaW9uLWNhcmR7XG4gICAgd2lkdGg6NzAlO1xuICAgIG1hcmdpbjphdXRvO1xufVxuaW9uLWljb257XG4gICAgem9vbTogMS4zO1xuICAgIG1hcmdpbi1yaWdodDoycHg7XG4gICAgY29sb3I6YmxhY2s7XG59XG5pb24tYnV0dG9ue1xuICAgIC0tY29sb3I6I2ZmZmZmZiAhaW1wb3J0YW50O1xufVxuaDF7XG4gICAgZm9udC1zaXplOjQwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG4vLyAubG9naW4tc2lkZXtcbi8vICAgICBiYWNrZ3JvdW5kOiB1cmwoRDpcXGthcmFuXFxUZXJyYWdvXFxzcmNcXGFzc2V0c1xcaW1hZ2VzXFxsb2dpbi1zaWRlLmpwZyk7XG4vLyAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuLy8gICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4vLyAgICAgYmFja2dyb3VuZC1jb2xvcjojZWVlZWVlO1xuLy8gfVxuaW9uLWltZ3tcbiAgICBoZWlnaHQ6IDQwMHB4O1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIG1hcmdpbjphdXRvO1xufVxuaW9uLWJ1dHRvblt0eXBlPSdzdWJtaXQnXXtcbiAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7XG4gICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xufVxuLnN1Ym1pdC1idG4ge1xuICAgIC0tYmFja2dyb3VuZDogI2ZmYjAxYTtcbiAgICAtLWNvbG9yOiAjMTYxYjEyO1xufVxuYnV0dG9uXG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjojMGNkMWU4O1xuICAgIGNvbG9yOndoaXRlO1xufVxuXG46Om5nLWRlZXAgLm1hdC1ob3Jpem9udGFsLXN0ZXBwZXItaGVhZGVyLWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLm1hdC1ob3Jpem9udGFsLWNvbnRlbnQtY29udGFpbmVyLC5tYXQtc3RlcHBlci1ob3Jpem9udGFsXG4gIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItYmFzZSk7XG4gIH1cblxuICAubWF0LWZvcm0tZmllbGRcbiAge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG4vKjpob3N0IHtcbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpO1xuICAgIH1cbn1cblxuLnBheiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDEwO1xufSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjc2MHB4KXtcbiAgICAubG9naW4tc2lkZXtcbiAgICAgICAgd2lkdGg6MTAwJTtcbiAgICB9XG4gICAgLnNob3djYXNle1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICB3aWR0aDowO1xuICAgIH1cbiAgICBpb24tY2FyZHtcbiAgICAgICAgd2lkdGg6IDk1JTtcbiAgICB9XG59XG4uYmxhbmt7XG4gICAgaGVpZ2h0OiAyMHZoO1xufVxuIiwiI2NvdmVyIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4zNSk7XG4gIHotaW5kZXg6IDU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUwMCU7XG4gIGN1cnNvcjogd2FpdDsgfVxuXG4ubWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoODklLCA0OSUpO1xuICB3aWR0aDogMzUlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDMlO1xuICBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTsgfVxuXG4ubmV3LWJhY2tncm91bmQtY29sb3Ige1xuICAtLWJhY2tncm91bmQ6ICNmZmIwMWE7IH1cblxuaW9uLWNvbnRlbnQge1xuICBmb250LWZhbWlseTogQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZiAhaW1wb3J0YW50OyB9XG5cbmlvbi10aXRsZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuaW9uLXRleHQsIGlvbi1jb2wge1xuICBtYXJnaW46IGF1dG87IH1cblxuLnNob3djYXNlIHtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgZ3JheTtcbiAgd2lkdGg6IDUwJSAhaW1wb3J0YW50OyB9XG5cbi5sb2dpbi1zaWRlIHtcbiAgd2lkdGg6IDUwJSAhaW1wb3J0YW50OyB9XG5cbmlvbi1jYXJkIHtcbiAgd2lkdGg6IDcwJTtcbiAgbWFyZ2luOiBhdXRvOyB9XG5cbmlvbi1pY29uIHtcbiAgem9vbTogMS4zO1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgY29sb3I6IGJsYWNrOyB9XG5cbmlvbi1idXR0b24ge1xuICAtLWNvbG9yOiNmZmZmZmYgIWltcG9ydGFudDsgfVxuXG5oMSB7XG4gIGZvbnQtc2l6ZTogNDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgb3BhY2l0eTogMC41OyB9XG5cbmlvbi1pbWcge1xuICBoZWlnaHQ6IDQwMHB4O1xuICB3aWR0aDogYXV0bztcbiAgbWFyZ2luOiBhdXRvOyB9XG5cbmlvbi1idXR0b25bdHlwZT0nc3VibWl0J10ge1xuICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7XG4gIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTsgfVxuXG4uc3VibWl0LWJ0biB7XG4gIC0tYmFja2dyb3VuZDogI2ZmYjAxYTtcbiAgLS1jb2xvcjogIzE2MWIxMjsgfVxuXG5idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMGNkMWU4O1xuICBjb2xvcjogd2hpdGU7IH1cblxuOjpuZy1kZWVwIC5tYXQtaG9yaXpvbnRhbC1zdGVwcGVyLWhlYWRlci1jb250YWluZXIge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7IH1cblxuLm1hdC1ob3Jpem9udGFsLWNvbnRlbnQtY29udGFpbmVyLCAubWF0LXN0ZXBwZXItaG9yaXpvbnRhbCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1iYXNlKTsgfVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTsgfVxuXG4vKjpob3N0IHtcbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpO1xuICAgIH1cbn1cblxuLnBheiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDEwO1xufSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjBweCkge1xuICAubG9naW4tc2lkZSB7XG4gICAgd2lkdGg6IDEwMCU7IH1cbiAgLnNob3djYXNlIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICAgIHdpZHRoOiAwOyB9XG4gIGlvbi1jYXJkIHtcbiAgICB3aWR0aDogOTUlOyB9IH1cblxuLmJsYW5rIHtcbiAgaGVpZ2h0OiAyMHZoOyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/login-with-otp/login-with-otp.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/login-with-otp/login-with-otp.page.ts ***!
  \*******************************************************/
/*! exports provided: LoginWithOtpPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginWithOtpPage", function() { return LoginWithOtpPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _Services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/authentication.service */ "./src/app/Services/authentication.service.ts");
/* harmony import */ var _Services_Login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Services/Login.service */ "./src/app/Services/Login.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Services_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Services/data.service */ "./src/app/Services/data.service.ts");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm5/snack-bar.es5.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");









var LoginWithOtpPage = /** @class */ (function () {
    function LoginWithOtpPage(authentication, loginservice, events, _formBuilder, data, _snackBar) {
        this.authentication = authentication;
        this.loginservice = loginservice;
        this.events = events;
        this._formBuilder = _formBuilder;
        this.data = data;
        this._snackBar = _snackBar;
        this.show = false;
        this.isLinear = false;
        this.events.publish('PageName', 'login');
    }
    LoginWithOtpPage.prototype.openSnackBar = function (message, action) {
        this._snackBar.open(message, action, {
            duration: 5000,
            // here specify the position
            verticalPosition: 'top'
        });
    };
    LoginWithOtpPage.prototype.doAsyncTask = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            setTimeout(function () {
                _this.authentication.loginWithOtp(localStorage.getItem('username'));
                resolve();
            }, 1000);
        });
        this.show = false;
        debugger;
        return promise;
    };
    LoginWithOtpPage.prototype.LoginWithOTP = function (OTP) {
        var _this = this;
        debugger;
        var OTPValue = localStorage.getItem('OTP');
        if (OTP == OTPValue) {
            this.show = true;
            localStorage.removeItem('OTP');
            this.doAsyncTask().then(function () { return _this.show = true; }, function () { return console.log("Task Errored!"); });
            //this.authentication.loginWithOtp(localStorage.getItem('username'));
            this.show = false;
        }
        else {
            //alert("OTP not valid");
            this.openSnackBar("Invalid OTP", null);
        }
    };
    LoginWithOtpPage.prototype.SendOtp = function (username) {
        var _this = this;
        this.loginservice.getUserDetailsWithoutPassword(username)
            .subscribe(function (data) {
            _this.User = data;
            console.log(_this.User);
            localStorage.setItem('username', _this.User.UserName);
            debugger;
            if (_this.User.Error != null) {
                _this.stepper.previous();
                _this.error = _this.User.Error;
                _this.openSnackBar(_this.error, null);
            }
            else {
                _this.loginservice.GenerateOTPEmail(_this.User.Email_id).subscribe(function (resp) {
                    alert(JSON.stringify(resp));
                    var OTP = resp["OTP"];
                    localStorage.setItem('OTP', OTP);
                });
            }
        }, function (error) {
            _this.error = error;
        });
        // debugger;
    };
    LoginWithOtpPage.prototype.getUserDetails = function (userid) {
    };
    LoginWithOtpPage.prototype.ngOnInit = function () {
        this.show = false;
        debugger;
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    LoginWithOtpPage.prototype.loginUser = function (e) {
        var _this = this;
        //debugger;
        e.preventDefault();
        console.log(e);
        var username = e.target.elements[0].value;
        var password = e.target.elements[1].value;
        this.loginservice.getUserDetails(username, password)
            .subscribe(function (data) {
            _this.User = data;
            console.log(_this.User);
            if (_this.User.Error != null) {
                _this.error = _this.User.Error;
            }
            else {
                _this.data.storage = {
                    "firstname": "Nic",
                    "lastname": "Raboy",
                    "address": {
                        "city": "San Francisco",
                        "state": "California"
                    }
                };
                _this.authentication.login(username, password);
            }
        }, function (error) {
            _this.error = error;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('stepper'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_stepper__WEBPACK_IMPORTED_MODULE_8__["MatStepper"])
    ], LoginWithOtpPage.prototype, "stepper", void 0);
    LoginWithOtpPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login-with-otp',
            template: __webpack_require__(/*! ./login-with-otp.page.html */ "./src/app/login-with-otp/login-with-otp.page.html"),
            styles: [__webpack_require__(/*! ./login-with-otp.page.scss */ "./src/app/login-with-otp/login-with-otp.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"], _Services_Login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Events"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _Services_data_service__WEBPACK_IMPORTED_MODULE_6__["Data"], _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_7__["MatSnackBar"]])
    ], LoginWithOtpPage);
    return LoginWithOtpPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-with-otp-login-with-otp-module.js.map